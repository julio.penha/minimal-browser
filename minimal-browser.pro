QT += core gui widgets webenginewidgets

CONFIG += c++11

TEMPLATE = app
TARGET = main

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui
