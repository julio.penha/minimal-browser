#include <QApplication>

#include "mainwindow.h"

int main(int argc, char *argv[]) {
  //  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QApplication a(argc, argv);
  MainWindow w;
  std::cout << "main()" << std::endl;
  w.show();
  return a.exec();
}
