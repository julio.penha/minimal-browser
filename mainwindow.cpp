#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QHBoxLayout>
#include <QUrl>
#include <QWebEngineView>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  std::cout << "show()" << std::endl;
  QWebEngineView *view = new QWebEngineView(this);

  view->setUrl(QUrl("https://platform.natosafe.com/"));
  view->setParent(ui->centralwidget);
  view->show();
  view->resize(this->size());
  int h_size = view->height();
  int w_size = view->width();
  std::cout << "show()" << std::endl;
  std::cout << "size = " << h_size << "x" << w_size << std::endl;
}

MainWindow::~MainWindow() { delete ui; }
